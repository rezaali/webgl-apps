# webgl-apps
WebGL Based Apps for Creating Customizable 3D Printable / Downloadable Objects in the Browser!

## Instructions

### 1. Run These Once To Install Environment Tools
```
npm install -g budo
npm install -g browserify
```

### 2. Install App Dependencies (inside app folder)
```
npm install
```

### 3. To Run the App (inside app folder)
```
npm start
```

## Notes

The app will automatically reload once you save any of the sketch files. To install other npm packages.
```
npm install --save [package]
```
