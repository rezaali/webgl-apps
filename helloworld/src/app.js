//WEBGL RELATED
var glContext = require('gl-context');
var glGeometry = require('gl-geometry');
var glShader = require('gl-shader');
var glslify = require('glslify');
var clear = require('gl-clear')( { color: [ 0.0, 0.0, 0.0, 1.0 ] } );

//MATH RELATED
var mat4 = require('gl-matrix').mat4;
var mat3 = require('gl-matrix').mat3;
var vec3 = require('gl-matrix').vec3;

//GEOMETRY RELATED
var lgp = require('lgp');
var mda = require('mda');

var Mesh = mda.Mesh;
var FaceVertices = mda.FaceVertices;
var InsertVertexOperator = mda.InsertVertexOperator;
var InsertEdgeOperator = mda.InsertEdgeOperator;
var DeleteEdgeOperator = mda.DeleteEdgeOperator;
var ExtrudeOperator = mda.ExtrudeOperator;
var PipeOperator = mda.PipeOperator;
var DuplicateOperator = mda.DuplicateOperator;
var CombineOperator = mda.CombineOperator;
var ScaleOperator = mda.ScaleOperator;
var MoveOperator = mda.MoveOperator;
var InvertOperator = mda.InvertOperator;
var MeshIntegrity = mda.MeshIntegrity;
var TriangulateOperator = mda.TriangulateOperator;
var LoopSmoothOperator = mda.LoopOperator;
var CatmullClarkOperator = mda.CatmullClarkOperator;
var MeshCentroid = mda.MeshCentroid;
var vertexNormals = require('guf').vertexNormals;
var calculateNormal = require('guf').calculateNormal;
var Camera = require('nsc');

//DOM RELATED
var keyPressed = require('key-pressed');
var fit = require('canvas-fit');
var isMobile = require('is-mobile');

var Samsara = require('samsarajs');
var EventHandler = Samsara.Events.EventHandler;

var Param = require('./../ui/Param');
//APP RELATED
// import App from './../../../libs/gaf';
import App from 'gaf';

var gl, canvas, cam;

var omesh, meshOut, meshOutTri;
var positions, cells;
var geoOut;
var geoWireOut, geoWire;
var geoPointsOut, geoPoints;

var color = [ 1.0, 1.0, 1.0, 1.0 ];
var colorPoints = [ 1.0, 1.0, 1.0, 0.5 ];
var colorWire = [ 1.0, 1.0, 1.0, 0.25 ];

var positionsOut;
var cellsOut;

class BoxApp extends App {
  constructor( props ) {
    super( props );
    cam = Camera( this.canvas, {
      position: [ 0.0, 0.0, -5.0 ],
      rotation: mat4.fromRotation( mat4.create(), Math.PI * 0.25, [ 1, 0, 0 ] )
    } );
    this.events = new EventHandler();
    this.params = {};
    this.setupParams();
    this.bindParams();
    this.rebuild = true;
  };

  getTitle = () => {
    return 'Hello World';
  };

  addParam = ( param ) => {
    this.params[ param.key ] = param;
  };

  getValue = ( key ) => {
    return this.params[ key ].getValue();
  };

  setValue = ( key, value ) => {
    return this.params[ key ].setValue( value );
  };

  setupParams = () => {
    this.addParam( new Param( { key: 'Reset', value: false, type: 'button' } ) );
    this.addParam( new Param( { key: 'Width', value: 2.0, min: 1.0, max: 10.0 } ) );
    this.addParam( new Param( { key: 'Height', value: 2.0, min: 0.1, max: 10.0 } ) );
    this.addParam( new Param( { key: 'Depth', value: 2.0, min: 1.0, max: 10.0 } ) );
    this.addParam( new Param( { key: 'Hole Depth', value: 1.9, min: 0.0001, max: 2.0 } ) );
    this.addParam( new Param( { key: 'Thickness', value: 0.2, min: 0.0001, max: 0.9 } ) );
    this.addParam( new Param( { key: 'Wireframe', value: false } ) );
    this.addParam( new Param( { key: 'Smoothness', value: 0, min: 0, max: 6, step: 1 } ) );
    this.addParam( new Param( { key: 'Top Crease', value: 0, min: 0, max: 6, step: 1 } ) );
    this.addParam( new Param( { key: 'Side Crease', value: 1, min: 1, max: 6, step: 1 } ) );
    this.addParam( new Param( { key: 'Bottom Crease', value: 0, min: 0, max: 6, step: 1 } ) );
    this.addParam( new Param( { key: 'Top Hole Crease', value: 0, min: 0, max: 6, step: 1 } ) );
    this.addParam( new Param( { key: 'Bottom Hole Crease', value: 0, min: 0, max: 6, step: 1 } ) );
  };

  getParams = () => {
    var params = this.params;
    return [
      params[ 'Reset' ],
      params[ 'Width' ],
      params[ 'Height' ],
      params[ 'Depth' ],
      params[ 'Hole Depth'],
      params[ 'Thickness' ],
      params[ 'Smoothness' ],
      params[ 'Wireframe' ],
      params[ 'Top Crease' ],
      params[ 'Side Crease' ],
      params[ 'Top Hole Crease' ],
      params[ 'Bottom Hole Crease' ],
      params[ 'Bottom Crease' ]
    ];
  };

  bindParams = () => {
    var keys = Object.keys( this.params );
    for( var i = 0; i < keys.length; i++ ) {
      this.events.subscribe( this.params[ keys[ i ] ] );
    }
    this.events.on( 'onChange', ( function( data ) {
      if( data.key === 'Reset' ) {
        if( data.value === true ) {
          var params = this.getParams();
          for( var i = 0; i < params.length; i++ ) {
            var param = params[ i ];
            if( param.key !== 'Reset' ) {
              param.reset();
            }
          }
        }
      }
      else if( data.key != 'Wireframe' ) {
        this.rebuild = true;
      }
      else if( data.key == 'Wireframe' ) {
        if( data.value ) {
          clear.color = [ 0, 0, 0, 1 ];
        }
        else {
          clear.color = [ 0, 0, 0, 0 ];
        }
      }
    } ).bind( this ) );
  };

  setup = () => { };

  setupGeometry = () => {
    var cpositions = [];
    var ccells = [];

    var w = this.getValue('Width') * 0.5;
    var h = this.getValue('Depth') * 0.5;
    var d = this.getValue('Height') * 0.5;
    var hd = this.getValue('Hole Depth') * d;
    var bd = this.getValue('Thickness') * Math.min( w, d );

    cpositions.push( vec3.fromValues( -w, -d, -h ) );
    cpositions.push( vec3.fromValues( w, -d, -h ) );
    cpositions.push( vec3.fromValues( w, -d, h ) );
    cpositions.push( vec3.fromValues( -w, -d, h ) );
    ccells.push( [ 3, 2, 1, 0 ], [ 0, 1, 2, 3 ] );

    omesh = new Mesh();
    omesh.setPositions( cpositions );
    omesh.setCells( ccells );
    omesh.process();
    //
    var sideness = this.getValue( 'Side Crease' );
    var sinc = ( d * 2.0 ) / sideness;
    for( var i = 0; i < sideness; i++ ) {
      ExtrudeOperator( omesh, 0, sinc, 0.0 );
    }

    var topness = this.getValue( 'Top Crease' );
    var rad = 0.001;
    for( var i = 0; i < topness; i++ ) {
      ExtrudeOperator( omesh, 0, 0.00, rad );
    }
    ExtrudeOperator( omesh, 0, 0.00, bd - ( topness * rad ) );

    var etcness = this.getValue( 'Top Hole Crease');
    for( var i = 0; i < etcness; i++ ) {
      ExtrudeOperator( omesh, 0, -rad, 0.0 );
    }
    ExtrudeOperator( omesh, 0, - ( hd - ( etcness * rad ) ), 0.0 );

    var holeness = this.getValue( 'Bottom Hole Crease');;
    for( var i = 0; i < holeness; i++ ) {
      ExtrudeOperator( omesh, 0, 0, rad );
    }

    var botness = this.getValue( 'Bottom Crease');;
    for( var i = 0; i < botness; i++ ) {
      ExtrudeOperator( omesh, 1, 0.0, rad );
    }
    ExtrudeOperator( omesh, 1, 0.00, bd - ( botness * rad ) );

    // MeshIntegrity( omesh );
    meshOut = undefined;
    meshOutTri = undefined;
    buildGeometry();
    var smoothness = this.getValue( 'Smoothness' );
    for( var i = 0; i < smoothness; i++ ) {
      this.smooth();
    }
  };

  smooth = () => {
    CatmullClarkOperator( meshOut );
    buildGeometry();
  };

  update = () => {
    if( this.rebuild ) {
      this.setupGeometry();
      this.rebuild = false;
    }
    width  = gl.drawingBufferWidth;
    height = gl.drawingBufferHeight;
    var aspectRatio = gl.drawingBufferWidth / gl.drawingBufferHeight;
    var fieldOfView = Math.PI / 4.0;
    var near = 0.01;
    var far  = 1000.0;
    mat4.perspective( projection, fieldOfView, aspectRatio, near, far );

    // get view from camera
    cam.view( view );
    cam.update();

    mat4.copy( normalm4, view );
    mat4.invert( normalm4, normalm4 );
    mat4.transpose( normalm4, normalm4 );
    mat3.fromMat4( normalm3, normalm4 );
    frame += 0.005;
  };

  render = () => {
    gl.viewport( 0, 0, width, height );
    clear( gl );
    if( this.getValue( 'Wireframe' ) ) {
      gl.disable( gl.DEPTH_TEST );
    }
    else {
      gl.enable( gl.DEPTH_TEST );
      drawGeo( geoOut );
    }
    drawGeoWireframe( geoWireOut );
    drawGeoPoints( geoPointsOut );
  };

  getPng = () => {
    return canvas.toDataURL('image/png');
  };

  savePng = ( filename ) => {
    let fn = filename.length ? filename : 'box';
    lgp.imageWriter( fn + '.png', canvas.toDataURL('image/png') );
  };

  saveObj = ( filename ) => {
    let fn = filename.length ? filename : 'box';
    lgp.fileWriter( fn + '.obj', lgp.objSerializer( { positions: positions, cells: cells } ) );
  };

  saveStl = ( filename ) => {
    let fn = filename.length ? filename : 'box';
    lgp.fileWriter( fn + '.stl', lgp.stlSerializer( { positions: positions, cells: cells } ) );
  };
};

var boxApp;
if( boxApp == undefined ) {
  boxApp = new BoxApp();
}

var gl = boxApp.getContext();
var canvas = boxApp.getCanvas();

function buildGeometry() {
  if( !meshOut ) {
    meshOut = DuplicateOperator( omesh );
  }

  geoWireOut = createGeoWire( meshOut.getPositions(), meshOut.getCells() );

  meshOutTri = DuplicateOperator( meshOut );
  TriangulateOperator( meshOutTri );

  positions = positionsOut = meshOutTri.getPositions();
  cells = cellsOut = meshOutTri.getCells();

  geoOut = createGeo( positionsOut, cellsOut );
  geoPointsOut = createGeoPoints( positionsOut );
}

function createGeo( positions, cells ) {
  var newPositions = [];
  var newNormals = [];

  for( var i = 0; i < cells.length; i++ ) {
    var a = positions[ cells[ i ][ 0 ] ];
    var b = positions[ cells[ i ][ 1 ] ];
    var c = positions[ cells[ i ][ 2 ] ];
    var n = calculateNormal( a, b, c );
    vec3.scale( n, n, 1.0 );
    newPositions.push( a, b, c );
    newNormals.push( n, n, n );
  }
  var geo = glGeometry( gl );
  geo.attr( 'aPosition', newPositions );
  geo.attr( 'aNormal', newNormals );
  return geo;
}

function createGeoWire( positions, cells ) {
  var lines = [];
  for( var i = 0; i < cells.length; i++ ) {
    var cell = cells[ i ];
    var clen = cell.length;
    for( var j = 0; j < clen; j++ ) {
      var i0 = cell[ j ];
      var i1 = cell[ ( j + 1 ) % clen ];
      lines.push( i0, i1 );
    }
  }

  var geoWire = glGeometry( gl );
  geoWire.attr( 'aPosition', positions );
  geoWire.faces( lines, { size: 2 } );
  return geoWire;
}

function createGeoPoints( positions ) {
  var geoPoints = glGeometry( gl );
  geoPoints.attr( 'aPosition', positions );
  return geoPoints;
}

// Set the canvas size to fill the window and its pixel density
window.addEventListener( 'resize', fit( boxApp.canvas, null, window.devicePixelRatio ), false );

// Setup Matricies
var projection = mat4.create();
var normalm4 = mat4.create();
var normalm3 = mat3.create();
var view = mat4.create();
var model = mat4.create();

// Setup Shaders
var vertexShader = glslify( './../shaders/shader.vert' );
var fragmentShader = glslify( './../shaders/shader.frag' );
var shader = glShader( gl, vertexShader, fragmentShader );

var vertexWireframeShader = glslify( './../shaders/shaderDebug.vert' );
var fragmentWireframeShader = glslify( './../shaders/shaderDebug.frag' );
var shaderDebug = glShader( gl, vertexWireframeShader, fragmentWireframeShader );

// Setup Sketch Variables
var height;
var width;
var frame = Math.PI;

function drawGeo( geo ) {
  if( geo ) {
    gl.enable( gl.BLEND );
    gl.blendFunc( gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA );
    geo.bind( shader );
    if( isMobile ) { shader.uniforms.dpr = window.devicePixelRatio * 2.0; } else { shader.uniforms.dpr = window.devicePixelRatio; }
    shader.uniforms.uPointSize = 1.0;
    shader.uniforms.uProjection = projection;
    shader.uniforms.uView = view;
    shader.uniforms.uNormalMatrix = normalm3;
    shader.uniforms.uModel = model;
    shader.uniforms.uColor = color;
    geo.draw( gl.TRIANGLES );
    geo.unbind();
  }
}

function drawGeoPoints( geoPoints ) {
  if( geoPoints ) {
    gl.enable( gl.BLEND );
    gl.blendFunc( gl.SRC_ALPHA, gl.ONE );
    geoPoints.bind( shaderDebug );
    if( isMobile ) { shaderDebug.uniforms.dpr = window.devicePixelRatio * 1.0; } else { shaderDebug.uniforms.dpr = 1.0; }
    shaderDebug.uniforms.uPointSize = 1.0;
    shaderDebug.uniforms.uProjection = projection;
    shaderDebug.uniforms.uView = view;
    shaderDebug.uniforms.uModel = model;
    shaderDebug.uniforms.uColor = colorPoints;
    geoPoints.draw( gl.POINTS );
    geoPoints.unbind();
  }
}

function drawGeoWireframe( geoWire ) {
  if( geoWire ) {
    gl.enable( gl.BLEND );
    gl.blendFunc( gl.SRC_ALPHA, gl.ONE );
    gl.lineWidth( 2.0 );
    geoWire.bind( shaderDebug );
    if( isMobile ) { shaderDebug.uniforms.dpr = window.devicePixelRatio * 2.0; } else { shaderDebug.uniforms.dpr = window.devicePixelRatio; }
    shaderDebug.uniforms.uPointSize = 1.0;
    shaderDebug.uniforms.uProjection = projection;
    shaderDebug.uniforms.uView = view;
    shaderDebug.uniforms.uModel = model;
    shaderDebug.uniforms.uColor = colorWire;
    geoWire.draw( gl.LINES );
    geoWire.unbind();
  }
}

export default boxApp;
