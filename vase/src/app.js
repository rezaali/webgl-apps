//WEBGL RELATED
var glContext = require('gl-context');
var glGeometry = require('gl-geometry');
var glShader = require('gl-shader');
var glslify = require('glslify');
var clear = require('gl-clear')( { color: [ 0.0, 0.0, 0.0, 1.0 ] } );

//MATH RELATED
var mat4 = require('gl-matrix').mat4;
var mat3 = require('gl-matrix').mat3;
var vec3 = require('gl-matrix').vec3;
var map = require('mhf').map;

//GEOMETRY RELATED
var lgp = require('lgp');
// var mda = require('./../../../libs/mda');
var mda = require('mda');
var Mesh = mda.Mesh;
var DuplicateOperator = mda.DuplicateOperator;
var TriangulateOperator = mda.TriangulateOperator;
var WireframeOperator = mda.WireframeOperator;
var CombineOperator = mda.CombineOperator;
var ScaleOperator = mda.ScaleOperator;
var MoveOperator = mda.MoveOperator;
var InvertOperator = mda.InvertOperator;
var PipeOperator = mda.PipeOperator;
var Moveperator = mda.MoveOperator;
var ExtrudeOperator = mda.ExtrudeOperator;

var eases = require('eases');
var easingsRadius = [ eases.linear, eases.backInOut, eases.backIn, eases.backOut, eases.bounceInOut, eases.bounceIn, eases.bounceOut, eases.circInOut, eases.circIn, eases.circOut, eases.cubicInOut, eases.cubicIn, eases.cubicOut, eases.elasticInOut, eases.elasticIn, eases.elasticOut, eases.expoInOut, eases.expoIn, eases.expoOut, eases.quadInOut, eases.quadIn, eases.quadOut, eases.quartInOut, eases.quartIn, eases.quartOut, eases.quintInOut, eases.quintIn, eases.quintOut, eases.sineInOut, eases.sineIn, eases.sineOut ];
var easingsHeight = [ eases.linear, eases.backInOut, eases.backIn, eases.backOut, eases.bounceInOut, eases.bounceIn, eases.bounceOut, eases.circInOut, eases.circIn, eases.circOut, eases.cubicInOut, eases.cubicIn, eases.cubicOut, eases.elasticInOut, eases.elasticIn, eases.elasticOut, eases.expoInOut, eases.expoIn, eases.expoOut, eases.quadInOut, eases.quadIn, eases.quadOut, eases.quartInOut, eases.quartIn, eases.quartOut, eases.quintInOut, eases.quintIn, eases.quintOut, eases.sineInOut, eases.sineIn, eases.sineOut ];

//RENDERING RELATED
var calculateNormal = require('guf').calculateNormal;
var Camera = require('nsc');

//DOM RELATED
var fit = require('canvas-fit');
var isMobile = require('is-mobile');
var Samsara = require('samsarajs');
var EventHandler = Samsara.Events.EventHandler;

// DATA RELATED
var Param = require('./../ui/Param');

//APP RELATED
import App from 'gaf';

// Setup Matricies
var projection = mat4.create();
var normalm4 = mat4.create();
var normalm3 = mat3.create();
var view = mat4.create();
var model = mat4.create();

// Setup Sketch Variables
var height;
var width;

// Variables
var gl, canvas, cam;

var omesh;
var positions;
var cells;
var geoOut;
var geoWireOut;
var geoPointsOut;
var color = [ 1.0, 1.0, 1.0, 1.0 ];
var colorPoints = [ 1.0, 1.0, 1.0, 0.5 ];
var colorWire = [ 1.0, 1.0, 1.0, 0.25 ];

class VaseApp extends App {
  constructor( props ) {
    super( props );
    this.events = new EventHandler();
    this.params = {};
    this.setupParams();
    this.bindParams();
    this.rebuild = true;
  };

  getTitle = () => {
    return 'Vase';
  };

  addParam = ( param ) => {
    return ( this.params[ param.key ] = param );
  };

  getParams = () => {
    var params = this.params;
    return [
      params[ 'Reset' ],
      params[ 'Facets' ],
      params[ 'Top Radius' ],
      params[ 'Bottom Radius' ],
      params[ 'Transition Radius' ],
      params[ 'Layers' ],
      params[ 'Height' ],
      params[ 'Transition Height' ],
      params[ 'Wireframe' ],
      params[ 'Open Top' ],
      params[ 'Open Bottom' ],
      params[ 'Thickness' ],
      params[ 'Depth' ],
      params[ 'Extrude Top' ],
      params[ 'Extrude Bottom' ],
      params[ 'Y Offset' ],
    ];
  };

  getValue = ( key ) => {
    return this.params[ key ].getValue();
  };

  setValue = ( key, value ) => {
    return this.params[ key ].setValue( value );
  };

  bindParams = () => {
    var keys = Object.keys( this.params );
    for( var i = 0; i < keys.length; i++ ) {
      this.events.subscribe( this.params[ keys[ i ] ] );
    }
    this.events.on( 'onChange', ( function( data ) {
      if( data.key === 'Reset' ) {
        if( data.value === true ) {
          var params = this.getParams();
          for( var i = 0; i < params.length; i++ ) {
            var param = params[ i ];
            if( param.key !== 'Reset' ) {
              param.reset();
            }
          }
        }
      }
      else if( data.key == 'Wireframe' ){
        if( data.value ) {
          clear.color = [ 0, 0, 0, 1 ];
        }
        else {
          clear.color = [ 0, 0, 0, 0 ];
        }
      }
      else {
        this.rebuild = true;
      }
    } ).bind( this ) );
  };

  setupParams = () => {
    this.addParam( new Param( { key: 'Open Top', value: true } ) );
    this.addParam( new Param( { key: 'Open Bottom', value: false } ) );
    this.addParam( new Param( { key: 'Facets', value: 9.0, min: 3, max: 36, step: 1 } ) );
    this.addParam( new Param( { key: 'Layers', value: 6.0, min: 2, max: 36, step: 1 } ) );
    this.addParam( new Param( { key: 'Top Radius', value: 1.0, min: 0.1, max: 5 } ) );
    this.addParam( new Param( { key: 'Bottom Radius', value: 1.0, min: 0.1, max: 5 } ) );
    this.addParam( new Param( { key: 'Transition Radius', value: 0, min: 0, max: easingsRadius.length - 1, step: 1 } ) );
    this.addParam( new Param( { key: 'Height', value: 2.75, min: 0.1, max: 5 } ) );
    this.addParam( new Param( { key: 'Transition Height', value: 0, min: 0, max: easingsHeight.length - 1, step: 1 } ) );
    this.addParam( new Param( { key: 'Thickness', value: 0.1, min: 0.1, max: 0.90 } ) );
    this.addParam( new Param( { key: 'Depth', value: 0.045, min: 0.01, max: 0.90 } ) );
    this.addParam( new Param( { key: 'Extrude Top', value: 0.0, min: 0.001, max: 5.0 } ) );
    this.addParam( new Param( { key: 'Extrude Bottom', value: 0.0, min: 0.001, max: 5.0 } ) );
    this.addParam( new Param( { key: 'Y Offset', value: 0.0, min: -1.0, max: 1.0 } ) );
    this.addParam( new Param( { key: 'Wireframe', value: false } ) );
    this.addParam( new Param( { key: 'Reset', value: false, type: 'button' } ) );
  };

  setup = () => {
    this.setupCamera();
  };

  setupCamera = () => {
    cam = Camera( this.canvas, {
      position: [ 0.0, -0.15, -5.0 ],
      // rotation: mat4.fromRotation( mat4.create(), Math.PI * 0.5, [ 1, 0, 0 ] )
      rotation: mat4.fromRotation( mat4.create(), Math.PI * 0.25, [ 1, 0, 0 ] )
    } );
  };

  setupGeometry = () => {
    var verts = [];
    var tris = [];

    var num = this.getValue( 'Facets' );
    var layers = this.getValue( 'Layers' );
    var height = 0.5 * this.getValue( 'Height' );
    var topRadius = this.getValue( 'Top Radius' );
    var botRadius = this.getValue( 'Bottom Radius' );
    var easingRadius = easingsRadius[ this.getValue( 'Transition Radius' ) ];
    var easingHeight = easingsHeight[ this.getValue( 'Transition Height' ) ];

    var inc = Math.PI * 2.0 / num;

    for( var j = 0; j < layers; j++ ) {
      var norm = j / ( layers - 1 );
      var radius = map( easingRadius( norm ), 0.0, 1.0, botRadius, topRadius );
      var zOffset = map( easingHeight( norm ), 0.0, 1.0, -height, height );
      var aOffset = j * Math.PI / num;
      for( var i = 0; i < num; i++ ) {
        verts.push( vec3.fromValues(
          radius * Math.sin( i * inc + aOffset ),
          zOffset,
          radius * Math.cos( i * inc + aOffset ) ) );
      }
    }

    // Top & Bottom Faces
    var top = [];
    var bot = [];
    var last = ( layers - 1 ) * num;
    for( var i = 0; i < num; i++ ) {
      top.push( last + i );
      bot.push( num - 1 - i );
    }
    tris.push( top );
    tris.push( bot );

    for( var j = 0; j < layers - 1; j++ ) {
      var curr = j * num;
      var next = ( j + 1 ) * num;
      for( var i = 0; i < num; i++ ) {
        var iNext = ( i + 1 ) % num;
        var i0 = curr + i;
        var i1 = curr + iNext;
        var i2 = next + i;
        var i3 = next + iNext;
        tris.push( [ i0, i1, i2 ] );
        tris.push( [ i1, i3, i2 ] );
      }
    }

    omesh = new Mesh();
    omesh.setPositions( verts );
    omesh.setCells( tris );
    omesh.process();

    var makeTop = this.getValue( 'Open Top' );
    var makeBottom = this.getValue( 'Open Bottom' );
    if( makeTop || makeBottom ) {
      var imesh = DuplicateOperator( omesh );
      var xzScale = 1.0 - this.getValue( 'Thickness' );
      var yScale = 1.0 - this.getValue( 'Depth' );
      var yOffset = this.getValue( 'Y Offset' ) * height;
      ScaleOperator( imesh, [ xzScale, yScale, xzScale ] );
      MoveOperator( imesh, [ 0.0, -yOffset, 0.0 ] );
      var extTop = this.getValue( 'Extrude Top' );
      var extBot = this.getValue( 'Extrude Bottom' );
      if( extTop > 0.001 ) {
        ExtrudeOperator( imesh, 0, extTop, 0.0 );
      }
      if( extBot > 0.001 ) {
        ExtrudeOperator( imesh, 1, extBot, 0.0 );
      }

      InvertOperator( imesh );
      var flenOuter = omesh.getFaces().length;
      var flenInner = imesh.getFaces().length;
      CombineOperator( omesh, imesh );

      if( makeTop ) {
        // ExtrudeOperator( omesh, 0, 0.0, 0.1 * Math.min( height / layers, 2 * Math.PI * radius / num ) );
        PipeOperator( omesh, 0, flenOuter + 0, -2 );
      }
      //
      if( makeBottom ) {
        // ExtrudeOperator( omesh, 1, 0.0, 0.1 * Math.min( height / layers, 2 * Math.PI * radius / num ) );
        PipeOperator( omesh, 1, flenOuter + 1, -2 );
      }
    }

    this.buildGeometry();
  };

  buildGeometry = () => {
    positions = omesh.getPositions();
    cells = omesh.getCells();
    geoWireOut = createGeoWire( positions, cells );

    var trimesh = new Mesh();
    trimesh.setPositions( positions );
    trimesh.setCells( cells );
    trimesh.process();
    TriangulateOperator( trimesh );

    positions = trimesh.getPositions();
    cells = trimesh.getCells();

    geoOut = createGeo( positions, cells );
    geoPointsOut = createGeoPoints( positions );
  };

  update = () => {
    if( this.rebuild ) {
      this.setupGeometry();
      this.rebuild = false;
    }

    width  = gl.drawingBufferWidth;
    height = gl.drawingBufferHeight;
    var aspectRatio = gl.drawingBufferWidth / gl.drawingBufferHeight;
    var fieldOfView = Math.PI / 4.0;
    var near = 0.01;
    var far  = 1000.0;
    mat4.perspective( projection, fieldOfView, aspectRatio, near, far );

    // get view from camera
    cam.view( view );
    cam.update();

    mat4.copy( normalm4, view );
    mat4.invert( normalm4, normalm4 );
    mat4.transpose( normalm4, normalm4 );
    mat3.fromMat4( normalm3, normalm4 );
  };

  render = () => {
    gl.viewport( 0, 0, width, height );
    clear( gl );
    if( this.getValue( 'Wireframe' ) ) {
      gl.disable( gl.DEPTH_TEST );
    }
    else {
      gl.enable( gl.DEPTH_TEST );
      drawGeo( geoOut );
    }

    drawGeoWireframe( geoWireOut );
    drawGeoPoints( geoPointsOut );
  };

  getPng = () => {
    return canvas.toDataURL('image/png');
  };

  savePng = ( filename ) => {
    let fn = filename.length ? filename : this.getTitle();
    lgp.imageWriter( fn + '.png', canvas.toDataURL('image/png') );
  };

  saveObj = ( filename ) => {
    let fn = filename.length ? filename : this.getTitle();
    lgp.fileWriter( fn + '.obj', lgp.objSerializer( { positions: positions, cells: cells } ) );
  };

  saveStl = ( filename ) => {
    let fn = filename.length ? filename : this.getTitle();
    lgp.fileWriter( fn + '.stl', lgp.stlSerializer( { positions: positions, cells: cells } ) );
  };
};

var app;
if( app == undefined ) {
  app = new VaseApp();
}

var gl = app.getContext();
var canvas = app.getCanvas();

// Set the canvas size to fill the window and its pixel density
var dpr = window.devicePixelRatio;
window.addEventListener( 'resize', fit( app.canvas, null, dpr ), false );

// Setup Shaders
var vertexShader = glslify( './../shaders/shader.vert' );
var fragmentShader = glslify( './../shaders/shader.frag' );
var shader = glShader( gl, vertexShader, fragmentShader );

var vertexWireframeShader = glslify( './../shaders/shaderDebug.vert' );
var fragmentWireframeShader = glslify( './../shaders/shaderDebug.frag' );
var shaderDebug = glShader( gl, vertexWireframeShader, fragmentWireframeShader );

function createGeo( positions, cells ) {
  var newPositions = [];
  var newNormals = [];

  for( var i = 0; i < cells.length; i++ ) {
    var a = positions[ cells[ i ][ 0 ] ];
    var b = positions[ cells[ i ][ 1 ] ];
    var c = positions[ cells[ i ][ 2 ] ];
    var n = calculateNormal( a, b, c );
    vec3.scale( n, n, 1.0 );
    newPositions.push( a, b, c );
    newNormals.push( n, n, n );
  }
  var geo = glGeometry( gl );
  geo.attr( 'aPosition', newPositions );
  geo.attr( 'aNormal', newNormals );
  return geo;
}

function createGeoWire( positions, cells ) {
  var lines = [];
  for( var i = 0; i < cells.length; i++ ) {
    var cell = cells[ i ];
    var clen = cell.length;
    for( var j = 0; j < clen; j++ ) {
      var i0 = cell[ j ];
      var i1 = cell[ ( j + 1 ) % clen ];
      lines.push( i0, i1 );
    }
  }

  var geoWire = glGeometry( gl );
  geoWire.attr( 'aPosition', positions );
  geoWire.faces( lines, { size: 2 } );
  return geoWire;
}

function createGeoPoints( positions ) {
  var geoPoints = glGeometry( gl );
  geoPoints.attr( 'aPosition', positions );
  return geoPoints;
}

function drawGeo( geo ) {
  if( geo ) {
    gl.enable( gl.BLEND );
    gl.blendFunc( gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA );
    geo.bind( shader );
    if( isMobile ) { shader.uniforms.dpr = dpr * 2.0; } else { shader.uniforms.dpr = dpr; }
    shader.uniforms.uPointSize = 1.0;
    shader.uniforms.uProjection = projection;
    shader.uniforms.uView = view;
    shader.uniforms.uNormalMatrix = normalm3;
    shader.uniforms.uModel = model;
    shader.uniforms.uColor = color;
    geo.draw( gl.TRIANGLES );
    geo.unbind();
  }
}

function drawGeoPoints( geoPoints ) {
  if( geoPoints ) {
    gl.enable( gl.BLEND );
    gl.blendFunc( gl.SRC_ALPHA, gl.ONE );
    geoPoints.bind( shaderDebug );
    if( isMobile ) { shaderDebug.uniforms.dpr = dpr * 1.0; } else { shaderDebug.uniforms.dpr = 1.0; }
    shaderDebug.uniforms.uPointSize = 1.0;
    shaderDebug.uniforms.uProjection = projection;
    shaderDebug.uniforms.uView = view;
    shaderDebug.uniforms.uModel = model;
    shaderDebug.uniforms.uColor = colorPoints;
    geoPoints.draw( gl.POINTS );
    geoPoints.unbind();
  }
}

function drawGeoWireframe( geoWire ) {
  if( geoWire ) {
    gl.enable( gl.BLEND );
    gl.blendFunc( gl.SRC_ALPHA, gl.ONE );
    gl.lineWidth( 2.0 );
    geoWire.bind( shaderDebug );
    if( isMobile ) { shaderDebug.uniforms.dpr = dpr * 2.0; } else { shaderDebug.uniforms.dpr = dpr; }
    shaderDebug.uniforms.uPointSize = 1.0;
    shaderDebug.uniforms.uProjection = projection;
    shaderDebug.uniforms.uView = view;
    shaderDebug.uniforms.uModel = model;
    shaderDebug.uniforms.uColor = colorWire;
    geoWire.draw( gl.LINES );
    geoWire.unbind();
  }
}

export default app;
